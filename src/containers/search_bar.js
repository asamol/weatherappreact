import React, { Component } from "react";
import { connect } from "react-redux";

export default class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = { term: "" };
    }
    render() {
        return (
            <form onSubmit={event => this.onFormSubmit(event)} className="input-group">
                <input
                    onChange={event => this.onInputChange(event)}
                    placeholder="Get a five-day forecast in your favorite cities"
                    className="form-control"
                    value={this.state.term}
                />
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">
                        Search
                    </button>
                </span>
            </form>
        );
    }

    onInputChange(event) {
        this.setState({ term: event.target.value });
    }

    onFormSubmit(event) {
        event.preventDefault();
    }
}
